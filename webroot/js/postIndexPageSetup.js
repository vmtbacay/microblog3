function postIndexSetup() {
    document.getElementsByClassName('sidebar')[0].style.height = (document.body.scrollHeight - 100) + 'px';

    $(".unlike").click(function() {
    unlike($(this));
    });
    $(".like").click(function(){
        like($(this));
    });
    $(".unrepost").click(function() {
        unrepost($(this));
    });
    $(".repost").click(function(){
        repost($(this));
    });

    $(".delete").click(function() {
        $("#deleteModal").css("display", "block");
        $(".deleteYes").attr("id", $(this).attr("id"));
        $(".deleteYes").unbind('click');

        $(".deleteYes").click(async function() {
            $.ajax({
                type: "POST",
                url: "/posts/delete/" + $(this).attr("id"),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                }
            });
            $(".deleteYes").unbind('click');
            $(window).unbind('click');
            await new Promise(resolve => setTimeout(resolve, 3000));
            location.reload();
        })
    })

    $(".deleteComment").click(function() {
        $("#deleteModal").css("display", "block");
        $(".deleteYes").attr("id", $(this).attr("id"));
        $(".deleteYes").unbind('click');

        $(".deleteYes").click(async function() {
            $.ajax({
                type: "POST",
                url: "/comments/delete/" + $(this).attr("id"),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                }
            });
            await new Promise(resolve => setTimeout(resolve, 3000));
            location.reload();
        })
    })

    $(window).click(function(event) {
        if (
            event.target == document.getElementById("deleteModal")
            || event.target == document.getElementsByClassName("deleteNo")[0]
        ) {
          $("#deleteModal").css("display", "none");
        }
    })

    $(".current").html($(".current").html() + " - You are here")

    $("form").submit(function (event) {
        $(":submit").attr("disabled", true);
    });
}

function unlike(me) {
    $.ajax({
        type: "POST",
        url: "/posts/unlike/" + me.attr("id"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        }
    });
    $("#" + me.attr("id") + ".unlike").unbind('click');
    $("#" + me.attr("id") + ".unlike").click(function() {
        like($(this));
    });
    $("#" + me.attr("id") + ".unlike").html("Like");
    $("#" + me.attr("id") + ".unlike").attr('class', 'like link');
    $("[id=likeCount-" + me.attr("id") + "]").html(parseInt($("[id=likeCount-" + me.attr("id") + "]").html()) - 1);
    $("[id$='Modal']").css("display", "none");
    $("#unlikeModal").css("display", "block");
    $("#unlikeModal").delay(1000)
        .fadeOut();
}

function like(me) {
    $.ajax({
        type: "POST",
        url: "/posts/like/" + me.attr("id"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        }
    });
    $("#" + me.attr("id") + ".like").unbind('click');
    $("#" + me.attr("id") + ".like").click(function() {
        unlike($(this));
    });
    $("#" + me.attr("id") + ".like").html("Unlike");
    $("#" + me.attr("id") + ".like").attr('class', 'unlike link');
    $("[id=likeCount-" + me.attr("id") + "]").html(parseInt($("[id=likeCount-" + me.attr("id") + "]").html()) + 1);
    $("[id$='Modal']").css("display", "none");
    $("#likeModal").css("display", "block");
    $("#likeModal").delay(1000)
        .fadeOut();
}

async function unrepost(me) {
    $.ajax({
        type: "POST",
        url: "/posts/unrepost/"+ me.attr("id"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        }
    });

    $("[id=repostCount-" + me.attr("id") + "]").html(parseInt($("[id=repostCount-" + me.attr("id") + "]").html()) - 1);
    $("[id$='Modal']").css("display", "none");
    $("#unrepostModal").css("display", "block");
    $("#unrepostModal").delay(1000)
        .fadeOut();
    $(".link").unbind('click');

    await new Promise(resolve => setTimeout(resolve, 2000));

    $.get(
        window.location.href,
        function(data, status) {
            let postsIndex = data.indexOf("<div id=\"posts\">");
            let posts = data.substring(postsIndex + 16);
            $("#posts").html(posts);

            postIndexSetup();
        }
    );
}

postIndexSetup();