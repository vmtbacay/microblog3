$("#pic").on("input", function() {
    var reader = new FileReader();
    reader.onload = function (e) {
        if (e.target.result.includes("image")) {
            document.getElementById("preview").src = e.target.result;
            $("#removeImage").show();
        } else {
            document.getElementById("preview").src = "/img/no_image.jpg";
            $("#removeImage").hide();
        }
    };
    if (document.getElementById("pic").files[0] == null) {
        document.getElementById("preview").src = "/img/no_image.jpg";
        $("#removeImage").hide();
    }
    reader.readAsDataURL(document.getElementById("pic").files[0]);
});

$("#removeImage").click(function() {
    document.getElementById("preview").src = "/img/no_image.jpg";
    document.getElementById("pic").value = "";
    $(this).hide();
});

$("#cancelAddPost").click(function () {
    $("#addPost").css("width", "190px");
    $("#addPost").css("max-height", "22px");
    $("#addPost").mouseenter(addPostMouseenter);
    $("#cancelAddPost").hide();
    document.getElementsByClassName("sidebar")[0].style.height = (document.body.scrollHeight - 670) + "px";
});

$("#addPost").mouseenter(addPostMouseenter);
$("#addPost").click(addPostClick);
$("#addPost").mouseleave(addPostMouseleave);

function addPostMouseenter() {
    $("#addPost").css("background-color", "gainsboro");
    $("#addPost").css("color", "dimgray");
    $("#addPost").css("cursor", "pointer");
    $("#addPost").click(addPostClick);
}

function addPostMouseleave() {
    $("#addPost").css("background-color", "white");
    $("#addPost").css("color", "black");
    $("#addPost").css("cursor", "default");
}

function addPostClick() {
    $("#addPost").css("transition", "width 1s, max-height 2s;");
    $("#addPost").css("width", "90%");
    $("#addPost").css("max-height", "1000px");
    $("#addPost").unbind("mouseenter");
    $("#addPost").unbind("click");
    addPostMouseleave();
    $("#cancelAddPost").show();
    document.getElementsByClassName("sidebar")[0].style.height = (document.body.scrollHeight + 475) + "px";
};