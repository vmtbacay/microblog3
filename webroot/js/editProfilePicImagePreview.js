$("#pic").on("input", function() {
    var reader = new FileReader();
    reader.onload = function (e) {
        if (e.target.result.includes("image")) {
            document.getElementById("preview").src = e.target.result;
        } else {
            document.getElementById("preview").src = "/img/no_image.jpg";
        }
    };
    if (document.getElementById("pic").files[0] == null) {
        document.getElementById("preview").src = "/img/no_image.jpg";
        $("#removeImage").hide();
    }
    reader.readAsDataURL(document.getElementById("pic").files[0]);
});