<?php
namespace App\View\Helper;

use Cake\View\Helper;

class SpaceHelper extends Helper {
    public function spaceMaker() {
        for ($i = 0; $i < 5; $i++) {
            echo '&nbsp;';
        }
    }
}