<?php
echo $this->Html->script('postIndexImagePreview', ['defer' => true]);

if (
    $viewUser['id'] === $this->getRequest()
        ->getSession()
        ->read('Auth.User.id')
    && $userOnly === null
) {
    ?>
    <div id="addPost">
        <span style="font-size: 20px;">Speak your mind!</span>
        <span id="cancelAddPost" class="link" style="float: right;" hidden>Cancel</span>
        <hr>
        <?php
        echo $this->Form->create($post, ['type' => 'file', 'url' => ['action' => 'add'], 'novalidate' => true]);
        echo $this->Form->control('title');
        echo $this->Form->control('body', ['rows' => '3']);
        ?>
        <span style="margin-left: 10px;">Image</span>
        <span style="display: flex; align-items: center;">
            <?= $this->Html->image(
                'no_image.jpg',
                ['height' => '100px', 'id' => 'preview', 'style' => 'margin: 10px;']
            ) ?>
            <button type="button" id="removeImage" hidden>Remove Image</button>
        </span>
        <?php
        echo $this->Form->control('pic', ['type' => 'file', 'label' => '']);
        echo $this->Form->submit('Save Post');
        echo $this->Form->end();
        ?>
    </div>
    <?php
} else {
    echo '<i style="font-size: 20px;">You are currently viewing <b>' . $viewUser['username'] . '</b>\'s page</i>';
    echo '<br><br>';
    echo '<h1 style="font-size: 20px;">Search through user\'s posts</h1>';
    echo '<hr>';
    echo $this->Form->create(
        null,
        ['url' => ['controller' => 'searches', 'action' => 'posts', $viewUser['id']], 'type' => 'get']
    );
    echo $this->Form->control('terms', ['label' => '', 'type' => 'text', 'style' => 'width: 200px']);
    echo $this->Form->submit('Search');
    echo $this->Form->end();
}

$postCount = count($allPosts);
$pages = max(ceil($postCount / PAGE_LIMIT), 1);
$page = min($pages, $this->request->getQuery('page') !== null ? $this->request->getQuery('page') : 1);
$offset = ($page - 1)  * PAGE_LIMIT;
$end = min(($offset + PAGE_LIMIT), $postCount);
?>
<br><br><br>

<h1 style="font-size: 20px;"><?= $userOnly === null ? 'Feed' : 'User\'s Posts' ?></h1>
<hr><br>
<div id="posts">
    <?php
    if ($postCount === 0) {
        ?>
        <div style="text-align: center;">
            <?= $this->Html->image('noPostsYet.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
        </div>
        <?php
    }
    for ($i = $offset; $i < $end; $i++) {
        $this->Putter->putPost(
            $allPosts[$i],
            $this->getRequest()
                ->getSession()
                ->read('Auth.User.follows')
        );
    }

    if ($pages > 1) {
        ?>
        <div style="text-align: center;">
            <?php
            $pageNumbers = range(1, $pages);
            unset($pageNumbers[$page-1]);

            $left = false;
            if ($page > 1) {
                $left = true;
                unset($pageNumbers[$page-2]);
            }
            $right = false;
            if ($page < $pages) {
                $right = true;
                unset($pageNumbers[$page]);
            }

            $first = 2;
            $count = 0;
            foreach ($pageNumbers as $number) {
                if ($number > $page - 1) {
                    break;
                }

                if ($count >= $first) {
                    echo "... | ";
                    break;
                }

                echo $this->Html->link($number, [
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    "?" => ['page' => $number]
                ]);
                echo  ' | ';
                unset($pageNumbers[$number-1]);
                $count++;
            }

            if ($left) {
                echo $this->Html->link($page - 1, [
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    "?" => ['page' => $page - 1]
                ]);
                echo  ' | ';
            }

            echo '<span class="current">' . $page . '</span>';

            if ($right) {
                echo  ' | ';
                echo $this->Html->link($page + 1, [
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    '?' => ['page' => $page + 1]
                ]);
            }

            $last = 2;
            while (!empty($pageNumbers && min($pageNumbers) < $page)) {
                array_shift($pageNumbers);
            }
            if (count($pageNumbers) > $last) {
                echo " | ...";
            }
            foreach (array_slice($pageNumbers, -$last, $last)  as $number) {
                if ($number > $page) {
                    echo  ' | ';
                    echo $this->Html->link($number, [
                        'controller' => 'posts',
                        'action' => 'index', $viewUser['id'], $userOnly,
                        '?' => ['page' => $number]
                    ]);
                }
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>