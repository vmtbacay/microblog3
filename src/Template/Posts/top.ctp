<h1 style="font-size: 20px;">Top 10 most liked posts in the last 24 hours</h1>
<hr>

<?php
if (empty($top)) {
    ?>
    <div style="text-align: center;">
        <?= $this->Html->image('noPostsYet.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
    </div>
    <?php
} else {
    foreach ($top as $post) {
        $this->Putter->putPost(
            $post, 
            $this->getRequest()
                ->getSession()
                ->read('Auth.User.follows')
        );
    }
}
?>