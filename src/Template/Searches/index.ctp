<h1 style="font-size: 20px;">Search Results</h1>
<hr><br>

<h1>Users with "<?= h($terms) ?>" in their username</h1>
<?php
if (empty($users)) {
    echo $this->Html->image('noUsers.png', ['height' => '200px']);
}
for ($i = 0; $i < min(RESULTS_LIMIT, count($users)); $i++) {
    $this->Putter->putUser($users[$i]);
}

if (count($users) > RESULTS_LIMIT) {
    echo $this->Html->link('More user results', [
        'controller' => 'searches',
        'action' => 'users',
        "?" => ['terms' => $terms]
    ]);
}
?>
<br><br><br>

<h1>Posts with "<?= h($terms) ?>" in their title or body</h1>
<?php
if (empty($posts)) {
    echo $this->Html->image('noPostsYet.png', ['height' => '200px']);
}
for ($i = 0; $i < min(RESULTS_LIMIT, count($posts)); $i++) {
    $this->Putter->putPost(
        $posts[$i],
        $this->getRequest()
            ->getSession()
            ->read('Auth.User.follows')
    );
}

if (count($posts) > RESULTS_LIMIT) {
    echo $this->Html->link('More post results', [
        'controller' => 'searches',
        'action' => 'posts',
        '?' => ['terms' => $terms]
    ]);
}
?>