<h1 style="font-size: 20px;">Post Search Results</h1>
<hr>

<h1>Posts with "<?= h($terms) ?>" in their title or body</h1>
<?php
if ($posts->isEmpty()) {
    ?>
    <div style="text-align: center;">
        <?= $this->Html->image('noPostsYet.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
    </div>
    <?php
} else {
    foreach ($posts as $post) {
        $this->Putter->putPost(
            $post,
            $this->getRequest()
                ->getSession()
                ->read('Auth.User.follows')
        );
    }
}
?>

<div style="text-align: center;">
    <?= $this->Paginator->numbers(
        ['first' => 2, 'modulus' => 2, 'last' => 2, 'before' => '<span hidden>']
    ) ?>
</div>