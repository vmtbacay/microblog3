<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div id="container" style="margin: 150px">
        <div style="float: left; margin-left: 120px; margin-top: 30px; max-width: 500px;">
            <div id="header">
                <h1><?= __('Uh oh.') ?></h1>
            </div>
            <div id="content">
                <?= $this->Flash->render() ?>

                <?= $this->fetch('content') ?>
            </div>
            <div id="footer">
                <?= $this->Html->link(__('Back'), 'javascript:history.back()') ?>
            </div>
        </div>
        <div style="float: right;">
            <?php
            echo $this->Html->image('error_robot.jpg', [
                'height' => '300px',
                'width' => '300px',
                'style' => 'margin-right: 120px;'
            ]);
            ?>
        </div>
    </div>
</body>
</html>
