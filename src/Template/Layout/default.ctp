<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $this->fetch('title') ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="/js/jquery-3.4.1.js"></script>
    <?php
    echo $this->Html->meta('icon');
    echo $this->HTML->css('microblog_3');
    echo $this->Html->script('postIndexPageSetup', ['defer' => true]);
    ?>
</head>
<body>
    <div class="topbar">
        <span class="home">
            <?php
            echo $this->Html->link(
                $this->Html->image('home.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Home</span>',
                '/posts',
                ['style' => 'text-decoration: none;', 'title' => 'Sweet Home', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('top.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Top Users</span>',
                ['controller' => 'users', 'action' => 'top'],
                ['style' => 'text-decoration: none;', 'title' => 'Kings and queens of micro', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('top.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Top Posts</span>',
                ['controller' => 'posts', 'action' => 'top'],
                ['style' => 'text-decoration: none;', 'title' => 'Quality posts only', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('fresh.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Fresh Users</span>',
                ['controller' => 'users', 'action' => 'fresh'],
                ['style' => 'text-decoration: none;', 'title' => 'Micronewbies', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('fresh.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Fresh Posts</span>',
                ['controller' => 'posts', 'action' => 'fresh'],
                ['style' => 'text-decoration: none;', 'title' => 'The latest from the people', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('random.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Random* User!</span>',
                ['controller' => 'users', 'action' => 'random'],
                ['style' => 'text-decoration: none;', 'title' => 'Jump into the unknown! *Cannot be taken to a user with no posts of their own!', 'escape' => false]
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('random.png', ['height' => '20px', 'width' => '20px'])
                . '<span style="display: flex-inline; vertical-align: top;"> Random Post!</span>',
                ['controller' => 'posts', 'action' => 'random'],
                ['style' => 'text-decoration: none;', 'title' => 'Jump into the unknown!', 'escape' => false]
            );
            ?>
        </span>
        <span style="float: right;">
            <table style="border-collapse: collapse; border-style: hidden;">
                <tr>
                    <?= $this->Form->create(
                        'Search', ['url' => ['controller' => 'searches', 'action' => 'index'], 'type' => 'get']
                    ) ?>
                    <td>
                        <?= $this->Form->control(
                            'terms',
                            ['label' => '', 'type' => 'text', 'style' => 'width: 200px']
                        ) ?>
                    </td>
                    <td>
                        <?= $this->Form->submit('Search') ?>
                    </td>
                        <?= $this->Form->end() ?>
                    <td>
                        <?= $this->Html->link(
                            'Log out',
                            ['controller' => 'posts', 'action' => 'logout'],
                            ['style' => 'text-decoration: none;']
                        ) ?>
                    </td>
                    <?= $this->Form->end(); ?>
                </tr>
            </table>
        </span>
    </div>
    <div class="sidebar">
        <?php
        $user = null;
        if (isset($viewUser)) {
            $user = $viewUser;
        } else {
            $user = $this->getRequest()
                ->getSession()
                ->read('Auth.User');
        }
        echo $this->Html->image($user['profile_pic'], ['height' => '150px', 'width' => '150px']);
        echo $this->Html->link(
            $user['username'],
            ['controller' => 'posts', 'action' => 'index', $user['id'], null],
            ['style' => 'color: white; font-size: 15px; text-decoration: underline;']
        );
        echo $this->Html->link('User\'s Posts', ['controller' => 'posts', 'action' => 'index', $user['id'], 1]);
        echo $this->Html->link('Followers', ['controller' => 'followers', 'action' => 'index', $user['id']]);
        echo $this->Html->link('Following', ['controller' => 'followers', 'action' => 'following', $user['id']]);
        if (
            $user['id'] === $this->getRequest()
                ->getSession()
                ->read('Auth.User.id')
        ) {
            echo $this->Html->link('Edit Username', ['controller' => 'users', 'action' => 'editUsername']);
            echo $this->Html->link('Edit Password', ['controller' => 'users', 'action' => 'editPassword']);
            echo $this->Html->link('Edit Email', ['controller' => 'users', 'action' => 'editEmail']);
            echo $this->Html->link('Edit Profile Picture', ['controller' => 'users', 'action' => 'editProfilePic']);
        } else {
            if (
                in_array(
                    $user['id'],
                    $this->getRequest()
                        ->getSession()
                        ->read('Auth.User.follows')
                )
            ) {
                echo $this->Form->postlink(
                    'Unfollow User',
                    ['controller' => 'followers', 'action' => 'unfollow', $user['id']]
                );
            } else {
                echo $this->Form->postlink(
                    'Follow User',
                    ['controller' => 'followers', 'action' => 'follow', $user['id']]
                );
            }
        }
        ?>
    </div>

    <div id="content" style="margin-left: 190px; padding: 30px;">
        <div id="deleteModal" class="modal">
            <div class="modal-content">
              <p>Are you sure you want to delete that?</p>
              <button class="deleteYes" style="width: 50px;">Yes</button>
              <button class="deleteNo" style="width: 50px;">No</button>
            </div>
        </div>

        <div id="likeModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Liked!</span>
            </div>
        </div>

        <div id="unlikeModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Unliked!</span>
            </div>
        </div>

        <div id="unrepostModal" class="modal-noti" style="width: 180px; height: 120px;">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Repost Deleted!</span>
            </div>
        </div>

        <?php
        echo $this->Flash->render();
        echo $this->fetch('content');
        ?>
    </div>
</body>
</html>