<?php
$this->Putter->putPost(
    $post,
    $this->getRequest()
        ->getSession()
        ->read('Auth.User.follows')
);
?>
<br>

<h1 style="font-size: 20px;">Comments</h1>
<hr>
<?php foreach ($comments as $comment): ?>
    <div style="border-style: ridge; margin: 10px; padding: 5px;  border-width: 2px; border-radius: 5px;">
        <div style="display: flex; align-items: center;">
            <?php
            echo $this->Html->image(
                $comment['user']['profile_pic'],
                ['height' => '50px', 'width' => '50px', 'style' => 'border-radius: 50%; border: 2px ridge']
            );
            $this->Space->spaceMaker();
            echo $this->Html->link(
                $comment['user']['username'],
                ['controller' => 'posts', 'action' => 'index', $comment['user']['id'], null],
                ['style' => 'text-decoration: none;']
            );
            echo '&nbsp;says:';
            ?>
        </div>
        <p style="margin: 10px"><?= h($comment['body']) ?></p>
        <div style="margin: 10px; display: flex; justify-content: space-between;">
            <span style="text-align: left; color: gray">
                Posted on: <?= h($comment['created']) ?>
            </span>
            <span>
                <?php
                if (
                    $comment['user_id'] === $this->getRequest()
                        ->getSession()
                        ->read('Auth.User.id')
                ) {
                    ?>
                    <div class="grow">
                        <?=
                        $this->Html->image(
                            'delete.jpg',
                            ['height' => '20', 'width' => '20', 'style' => 'vertical-align: top;']
                        );
                        ?>
                        <span class="deleteComment link" id="<?= $comment['id'] ?>">
                            Delete
                        </span>
                    </div>
                    &nbsp;

                    <div class="grow">
                        <?php
                        echo $this->Html->image(
                            'edit.png',
                            ['height' => '15', 'width' => '15', 'style' => 'vertical-align: middle;']
                        );
                        echo ' ';
                        echo $this->Html->link(
                            'Edit',
                            ['controller' => 'comments', 'action' => 'edit', $comment['id']],
                            ['style' => 'text-decoration: none;']
                        );
                        ?>
                    </div>
                    <?php
                }
                ?>
            </span>
        </div>
    </div>
<?php endforeach; ?>

<div style="text-align: center;">
    <?= $this->Paginator->numbers(
        ['first' => 2, 'last' => 2, 'modulus' => 2, 'before' => '<span hidden>']
    ) ?>
</div>

<?php
echo $this->Form->create($comm, ['url' => ['action' => 'add', $id], 'novalidate' => true]);
echo $this->Form->control('body', ['rows' => '3', 'label' => 'Add Comment']);
echo $this->Form->submit('Submit Comment');
echo $this->Form->end();
?>