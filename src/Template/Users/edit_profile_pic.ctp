<?= $this->Html->script('editProfilePicImagePreview', ['defer' => true]) ?>
<h1 style="font-size: 20px;">Edit Profile Picture</h1>
<hr>

<span style="display: flex; align-items: center;">
    <?= $this->Html->image(
        'no_image.jpg',
        ['height' => '150px', 'width' => '150px', 'id' => 'preview', 'style' => 'margin: 10px;']
    ) ?>
</span>
<?php
echo $this->Form->create($user, ['type' => 'file']);
echo $this->Form->file('pic', ['type' => 'file', 'label' => '', 'id' => 'pic']);
echo $this->Form->submit('Upload Profile Picture');
echo $this->Form->end();
?>