<h1 style="font-size: 20px;">Edit Email</h1>
<hr>

<?php
echo $this->Form->create($user, ['novalidate' => true]);
echo $this->Form->control('email', ['label' => 'New Email', 'value' => '']);
echo $this->Form->submit('Save Email');
echo $this->Form->end();
?>