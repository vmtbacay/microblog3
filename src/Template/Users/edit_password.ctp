<h1 style="font-size: 20px;">Edit Password</h1>
<hr>

<?php
echo $this->Form->create($user, ['novalidate' => true]);
echo $this->Form->control('oldpass', ['label' => 'Old Password', 'type' => 'password', 'value' => '']);
echo '<br>';
echo $this->Form->control('password', ['label' => 'New Password', 'value' => '']);
echo $this->Form->control('repass', ['label' => 'Re-enter New Password', 'type' => 'password', 'value' => '']);
echo $this->Form->submit('Save Password');
echo $this->Form->end();
?>