<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table {
    public function initialize(array $config) {
        $this->hasMany('Followers')
            ->setConditions(['Followers.deleted' => '0']);
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('username', 'Username cannot be empty')
            ->alphaNumeric('username', 'Username must be letters and numbers only')
            ->lengthBetween('username', [5, 15], 'Username must be between 5 to 15 characters')
            ->add(
                'username',
                ['unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Username already taken'
                ]]
            );

        $validator->notEmpty('password', 'Password cannot be empty')
            ->alphaNumeric('password', 'Password must be etters and numbers only')
            ->minLength('password', 8, 'Password must be at least 8 characters')
            ->sameAs('repass', 'password', 'Re-entered password not the same')
            ->allowEmpty('repass');

        $validator->notEmpty('email', 'Email cannot be empty')
            ->email('email', false, 'Invalid email')
            ->add(
                'email',
                ['unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Email already taken'
                ]]
            );

        return $validator;
    }
}