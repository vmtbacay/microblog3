<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table {
    public function initialize(array $config) {
        $this->belongsTo('Users');
        $this->belongsTo('RPCs', ['className' => 'Posts'])
            ->setForeignKey('repost_id');

        $this->hasMany('Reposts')
            ->setConditions(['Reposts.deleted' => '0']);
        $this->hasMany('Likes')
            ->setConditions(['Likes.deleted' => '0']);
        $this->hasMany('Comments')
            ->setConditions(['Comments.deleted' => '0']);
        $this->hasMany('RPCPosts', ['className' => 'Posts'])
            ->setForeignKey('repost_id')
            ->setConditions(['RPCPosts.deleted' => '0']);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('title', 'Post title cannot be empty')
            ->maxLength('title', 50, 'Post title must be between 1 to 50 characters long (inclusive)');

        $validator
            ->allowEmpty('body')
            ->maxLength('body', 140, 'Post body be between 1 to 140 characters long (inclusive)');

        return $validator;
    }
}