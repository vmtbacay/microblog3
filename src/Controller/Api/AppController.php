<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }

    public function beforeFilter(Event $event) {
        try {
            if (empty($this->request->getHeader('x-api-key'))) {
                $status = ['message' => 'No key', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                $this->setAction('pass');
            } else {
                $key = (new Parser())->parse($this->request->getHeader('x-api-key')[0]);

                if (!$key->verify(new Sha256(), 'microblogKey')) {
                    $status = ['message' => 'Invalid key', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    $this->setAction('pass');
                }
            }
        } catch (RuntimeException | InvalidArgumentException $e) {
            $status = ['message' => 'Invalid key', 'code' => '401'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            $this->setAction('pass');
        }
    }
}