<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class UsersController extends AppController {
    public function initialize() {
        parent::initialize();
    }
}