<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class CommentsController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->loadModel('Posts');
        $this->loadModel('Comments');
        $this->loadModel('Followers');

        $this->loadComponent('Flash');
    }

    public function index($id = null) {
        if (!$id) {
            throw new Exception(__('Invalid post'));
        }

        $post = $this->Posts->findByIdAndDeleted($id, 0)
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->first();
        if ($post['r_p_c'] !== null) {
            $post['r_p_c'] = $this->Posts->find('all', ['conditions' => ['Posts.id' => $post['r_p_c']['id']]])
                ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
                ->first();
        }
        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid post'));
        }

        $this->set('comm', $this->Comments->newEntity());
        $this->set('id', $id);
        $this->set('post', $post);

        $this->paginate = [
            'conditions' => ['Comments.post_id' => $id, 'Comments.deleted' => 0],
            'limit' => PAGE_LIMIT,
            'order' => ['Comments.created' => 'desc'],
            'contain' => 'Users'
        ];
        try {
            $this->set('comments', $this->paginate($this->Comments));
        } catch (Exception $e) {
            return $this->redirect(array_merge(
                ['action' => 'index', $id],
                ['page' => ceil($this->Comments->findAllByPostIdAndDeleted($id, 0)->count() / PAGE_LIMIT)]
            ));
        }

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'post', 'comments']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
    }

    public function add($id) {
        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $post = $this->Posts->findByIdAndDeleted($id, 0)
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->first();
        if ($post['r_p_c'] !== null) {
            $post['r_p_c'] = $this->Posts->find('all', ['conditions' => ['Posts.id' => $post['r_p_c']['id']]])
                ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
                ->first();
        }

        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        if ($this->request->is('post')) {
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (!$token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (empty($this->request->getdata()['body'])) {
                    $status = ['message' => 'Body cannot be empty', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }

            $request = $this->request->getData();
            $request['user_id'] = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            $request['post_id'] = $id;

            $comment = $this->Comments->newEntity($request);
            if (empty($comment->body)) {
                $comment->setError('body', ['Body cannot be empty']);
            }

            if ($this->Comments->save($comment)) {
                if ($this->request->is('json')) {
                    $comment = $this->Comments->findById($comment->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'comment']));
                    $this->set('_serialize', ['status', 'comment']);
                    return;
                }

                $this->Flash->success(__('Your comment has been added.'));
                return $this->redirect(['action' => 'index', $id]);
            }
            if ($comment->getErrors()) {
                if ($this->request->is('json')) {
                    $error_msg = [];
                    foreach ($comment->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }
                    $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                foreach ($comment->getErrors() as $errors) {
                    $this->Flash->error(__(implode(', ', $errors)));
                }
            }
            return $this->redirect($this->referer());
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function edit($id) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['body'])) {
                $status = ['message' => 'Body cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid comment', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid comment'));
        }

        $comment = $this->Comments->findByIdAndDeleted($id, 0)->first();

        if (!$comment) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid comment', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid comment'));
        }

        $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
        if ($comment['user_id'] != $user_id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid comment', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            $this->Flash->error(__('Unable to edit others\' comments.'));
            return $this->redirect(['action' => 'index', $comment->post_id]);
        }

        $this->set('comment', $comment);

        if ($this->request->is(['post', 'put'])) {
            $request = $this->request->getData();
            $request['modified'] = date("Y-m-d H:i:s");

            $comment = $this->Comments->patchEntity($comment, $request);
            if (empty($comment->body)) {
                $comment->setError('body', ['Body cannot be empty']);
            }

            if ($this->Comments->save($comment)) {
                if ($this->request->is('json')) {
                    $comment = $this->Comments->findById($comment->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'comment']));
                    $this->set('_serialize', ['status', 'comment']);
                    return;
                }

                $this->Flash->success(__('Your comment has been updated.'));
                return $this->redirect(['action' => 'index', $comment->post_id]);
            }
            if ($comment->getErrors()) {
                if ($this->request->is('json')) {
                    $error_msg = [];
                    foreach ($comment->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }
                    $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                foreach ($comment->getErrors() as $errors) {
                    $this->Flash->error(__(implode(', ', $errors)));
                }
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function delete($id) {
        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid comment', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid comment'));
        }

        $comment = $this->Comments->findByIdAndDeleted($id, 0)
            ->first();
        if (!$comment) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid comment', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid comment'));
        }

        if ($this->request->is('post')) {
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (!$token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (empty($this->request->getdata()['body'])) {
                    $status = ['message' => 'Body cannot be empty', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }

            $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            if ($comment['user_id'] != $user_id) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Unable to delete others\' comments', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('Unable to delete others\' comments'));
            }

            $comment->deleted = 1;
            $comment->deleted_date = date("Y-m-d H:i:s");
            if ($this->Comments->save($comment)) {
                if ($this->request->is('json')) {
                    $comment = $this->Comments->findById($comment->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'comment']));
                    $this->set('_serialize', ['status', 'comment']);
                    return;
                }

                $this->Flash->success(__('Your comment has been deleted.'));
                return $this->redirect($this->referer());
            } else {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Unable to delete your comment', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                $this->Flash->error(__('Unable to delete your comment'));
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        } else {
            throw new Exception(__('Method not allowed'));
        }
        return $this->redirect($this->referer());
    }
}