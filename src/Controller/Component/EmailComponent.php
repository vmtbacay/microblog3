<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;

class EmailComponent extends Component {
    public function sendMail($to, $subject, $message) {
        TransportFactory::setConfig('gmail', [
            'host' => 'ssl://smtp.gmail.com',
            'port' => 465,
            'username' => 'ynsmicroblog@gmail.com',
            'password' => 'microbloG0',
            'className' => 'Smtp'
        ]);
        $email = new Email('default');
        $email->setTransport('gmail');
        $email->setFrom(['code@microblog.com' => 'microblog'])
            ->setTo($to)
            ->setSubject($subject)
            ->send($message);
    }
}